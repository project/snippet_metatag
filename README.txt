Snippet metatag
===============

Description
-----------
This module extends the Snippet manager module by adding support for meta tags
configuration for snippet pages.

Configuration
-------------
Once the module enabled the an additional tab called 'Metatags' should appear on
snippet edit form.

Links
-------------
Project page: https://www.drupal.org/project/snippet_metatag
